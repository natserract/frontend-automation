# CI/CD pipeline

This is an official starter Turborepo.

## What's inside:
- [ ] circle ci https://circleci.com/ (PENDING, account suspended https://support.circleci.com/hc/en-us/articles/13253058649883-My-Project-Has-Been-Suspended)
- [x] github actions https://github.com/features/actions
- [x] renovate https://www.mend.io/renovate/
- [x] pnpm https://pnpm.io/
- [x] dangerjs https://github.com/danger/danger-js
- [x] husky https://typicode.github.io/husky/
- [x] argos-ci https://argos-ci.com/
